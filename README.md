# StarWarsLandia website project
> The project is a website of amusement park based on Star Wars world 

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Contact](#contact)
<!-- * [License](#license) -->


## General Information
- This project was realized V semester of studies.
- Website is imaginary
- The purpose of project was to learn scripts like JavaScript and also HTML, PHP, CSS.
- Project contains all files needed to run the website


## Technologies Used
- HTML 5
- PHP 5
- MySQL (database)
- CSS for stylesheet
- Brackets (editor)


## Features
Provided features:
- Login/Register function
- Admin panel
- Small forum
- A several form that can be send to owner
- Image galery
- Editind page and content added by user
- Reservation system
- JavaScript alerts
- Catches errors


## Project Status
Project is: _no longer being worked on_. 


## Room for Improvement
To do:
- Rewrite HTML and PHP to use some template
- Make code clear to read (sphagetti code for now)
- Write PHP objects
- Add database new functions



## Contact
Created by Piotr Hajduk

